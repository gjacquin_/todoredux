//
//  Store.swift
//  TodoRedux
//
//  Created by Grégoire Jacquin on 25/06/2019.
//  Copyright © 2019 Grégoire Jacquin. All rights reserved.
//

import SwiftUI
import Combine

class Store: BindableObject {
    public let didChange = PassthroughSubject<Store,Never>()
    
    private var currentState: [TodoItem]
    private var reducer: TodoReducer
    
    var state: [TodoItem] { currentState }
    
    init(initialState: [TodoItem]) {
        self.currentState = initialState
        self.reducer = TodoReducer()
    }
    
    func dispatch(action: TodoAction)
    {
        let newState = reducer.reducer(oldState: currentState, action: action)
        currentState = newState
        
        didChange.send(self)
    }
}
