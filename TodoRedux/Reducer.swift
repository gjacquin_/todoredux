//
//  Reducer.swift
//  TodoRedux
//
//  Created by Grégoire Jacquin on 25/06/2019.
//  Copyright © 2019 Grégoire Jacquin. All rights reserved.
//
import Foundation

enum TodoAction {
    case addTodo(String)
    case markCompleted(TodoItem)
    case deleteTodo(IndexSet)
}

struct TodoReducer {
    func reducer(oldState: [TodoItem],action: TodoAction) -> [TodoItem]
    {
        switch action
        {
        case .addTodo(let name) :
            let item = TodoItem(id: oldState.count, name: name)
            var newState = oldState
            newState.append(item)
            return newState
            
        case .markCompleted(let item):
            var newState = oldState
            var newItem = item
            let i = newState.firstIndex { item.id == $0.id }
            newItem.markCompleted()
            newState.insert(newItem, at: i!)
            return newState
            
        case .deleteTodo(let indexSet):
            guard let index = indexSet.first, index < oldState.count else { return oldState}
            var newState = oldState
            newState[index].markDeleted()
            return newState
        }
    }
}
