//
//  ContentView.swift
//  TodoRedux
//
//  Created by Grégoire Jacquin on 25/06/2019.
//  Copyright © 2019 Grégoire Jacquin. All rights reserved.
//

import SwiftUI
struct ContentView : View {
    
    let store = Store(initialState: [TodoItem(id: 0, name: "Try SwiftUI!")])
    
    var body: some View {
        NavigationView {
            TodoList().environmentObject(store)
            }.navigationBarTitle(Text("Todos"))
    }
}

struct TodoList: View {
    @EnvironmentObject
    var store: Store
    
    var body: some View {
        return List {
            AddTodoRow()
            ForEach(self.store.state.filter({ !($0.isDeleted) }).identified(by: \.id)) { item in
                NavigationButton(destination: Text("\(item.name)")) {
                    Text("\(item.name)")
                }
                }
                .onDelete(perform: delete)
        }
    }
    
    func delete(at offsets: IndexSet) {
        store.dispatch(action: TodoAction.deleteTodo(offsets))
    }
}

struct AddTodoRow: View  {
    
    @EnvironmentObject
    var store: Store
    
    @State
    private var todoName: String = ""
    
    @State
    private var addMode: Bool = false
    
    var body: some View {
        HStack {
            if self.addMode {
                TextField($todoName, placeholder: Text("Type here..."))
                    .textFieldStyle(.roundedBorder)
                
                Button(action: {
                    self.store.dispatch(action: TodoAction.addTodo(self.todoName))
                    self.todoName = ""
                    self.addMode = false
                }, label: {
                    Text("Add")
                })
            }
            else {
                Button.init(action: {
                    self.addMode = true
                }) {
                    HStack {
                        Image(systemName: "plus.circle.fill")
                        Text("Add a todo")
                        }.foregroundColor(Color.blue)
                    
                }
            }
            
        }
    }
}
