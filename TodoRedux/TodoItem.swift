//
//  TodoItem.swift
//  TodoRedux
//
//  Created by Grégoire Jacquin on 25/06/2019.
//  Copyright © 2019 Grégoire Jacquin. All rights reserved.
//

import SwiftUI

struct TodoItem : Identifiable {
    let id: Int
    let name: String
    var isCompleted = false
    var isDeleted = false
    
    mutating func markCompleted() {
        isCompleted = true
    }
    mutating func markDeleted() {
        isDeleted = true
    }
}
